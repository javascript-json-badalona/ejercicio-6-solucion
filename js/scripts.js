const listaPacientes = [
  {
    paciente: {
      nombre: 'Manel',
      edad: 25,
      sexo: 'H'
    },
    diasIngresado: 3,
    dieta: 'Baja en fibra'
  },
  {
    paciente: {
      nombre: 'Marta',
      edad: 56,
      sexo: 'M'
    },
    diasIngresado: 5,
    dieta: 'Diabetes tipo I'
  },
  {
    paciente: {
      nombre: 'Júlia',
      edad: 38,
      sexo: 'M'
    },
    diasIngresado: 1,
    dieta: 'Sin sal'
  },
  {
    paciente: {
      nombre: 'Esteve',
      edad: 40,
      sexo: 'H'
    },
    diasIngresado: 3,
    dieta: 'Diabetes tipo II'
  },
  {
    paciente: {
      nombre: 'Arturo',
      edad: 19,
      sexo: 'H'
    },
    diasIngresado: 2,
    dieta: 'Baja en fibra'
  },
  {
    paciente: {
      nombre: 'Isabel',
      edad: 63,
      sexo: 'M'
    },
    diasIngresado: 6,
    dieta: 'Sin sal'
  }
];

function infoPacientes(pacientes) {
  const nPacientes = pacientes.length;
  const nMayoresEdad = pacientes.filter(p => p.paciente.edad >= 18).length;
  const nHombresDiabeticos = pacientes.filter(p => p.paciente.sexo === 'H' && p.dieta.toLowerCase().includes('diabetes')).length;
  const nDiasIngreso = pacientes.reduce((acc, p) => acc + p.diasIngresado, 0);
  const edadMediaMujeres = pacientes.filter(p => p.paciente.sexo === 'M').reduce((acc, p, i, mujeres) => acc + p.paciente.edad / mujeres.length, 0).toFixed(2);
  const info = {
    nPacientes,
    nMayoresEdad,
    nHombresDiabeticos,
    nDiasIngreso,
    edadMediaMujeres
  };
  return info;
}

console.log(infoPacientes(listaPacientes));
